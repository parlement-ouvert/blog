import React from 'react'
import { Link, graphql } from 'gatsby'
import Img from 'gatsby-image'
import MDXRenderer from 'gatsby-mdx/mdx-renderer'

import Bio from '../components/Bio'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import Tag from "../components/tag"
import { rhythm, scale } from '../utils/typography'

class BlogPostTemplate extends React.Component {
    render() {
        const post = this.props.data.mdx
        const siteTitle = this.props.data.site.siteMetadata.title
        const { previous, next } = this.props.pageContext

        return (
            <Layout location={this.props.location} title={siteTitle}>
                <SEO title={post.frontmatter.title} description={post.excerpt} />
                <h1>{post.frontmatter.title}</h1>
                <p>
                    Par {post.frontmatter.author} le {post.frontmatter.date}
                </p>

                <p>
                    {post.frontmatter.tags.map((tag, index) => {
                        return <Tag key={index} text={tag} />
                    })}
                </p>

                {post.frontmatter.featuredImage ?
                    <Img sizes={post.frontmatter.featuredImage.childImageSharp.sizes} /> :
                    null
                }

                <MDXRenderer>{post.code.body}</MDXRenderer>

                <hr style={{ marginBottom: rhythm(1), }} />

                <ul
                    style={{
                        display: `flex`,
                        flexWrap: `wrap`,
                        justifyContent: `space-between`,
                        listStyle: `none`,
                        padding: 0,
                    }}
                >
                    <li>
                        {previous && (
                            <Link to={previous.fields.slug} rel="prev">
                            ← {previous.frontmatter.title}
                            </Link>
                        )}
                    </li>
                    <li>
                        {next && (
                            <Link to={next.fields.slug} rel="next">
                            {next.frontmatter.title} →
                            </Link>
                        )}
                    </li>
                </ul>

                <hr/>

                <p>
                    Envie de commenter ou de contribuer ? Postez vos réactions sur le <a href="https://forum.parlement-ouvert.fr">Forum du Bureau Ouvert</a>.
                </p>
            </Layout>
        )
    }
}

export default BlogPostTemplate

export const pageQuery = graphql`
query($slug: String!) {
    site {
        siteMetadata {
            title
            author
            social {
                url
                twitter
            }
        }
    }
    mdx(fields: { slug: { eq: $slug } }) {
        id
        excerpt(pruneLength: 200)
        fields {
            slug
        }
        frontmatter {
            title
            author
            tags
            date(formatString: "DD MMMM YYYY", locale: "fr")
            featuredImage {
                childImageSharp{
                    sizes(maxWidth: 630) {
                        ...GatsbyImageSharpSizes
                    }
                }
            }
        }
        code {
            body
        }
    }
}
`
