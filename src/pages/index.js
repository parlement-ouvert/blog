import React from 'react'
import { Link, graphql } from 'gatsby'
import Img from 'gatsby-image'

import Layout from '../components/Layout'
import SEO from '../components/seo'
import Tag from "../components/tag"

import { rhythm } from '../utils/typography'

class BlogIndex extends React.Component {
    render() {
        const { data } = this.props
        const siteTitle = data.site.siteMetadata.title
        const posts = data.allMdx.edges

        return (
            <Layout location={this.props.location} title={siteTitle}>
            <SEO
                title="Accueil"
                keywords={[`bureau ouvert`, `blog`, `assemblée nationale`]}
            />

            {posts.map(({ node }) => {
                const title = node.frontmatter.title || node.fields.slug

                return (
                    <div key={node.fields.slug}>
                    <h3
                        style={{
                            marginBottom: rhythm(1 / 4),
                        }}
                    >
                        <Link style={{ boxShadow: `none` }} to={node.fields.slug}>{title}</Link>
                    </h3>

                    <small>
                        Par {node.frontmatter.author} le {node.frontmatter.date}
                    </small>

                    <p>
                        {node.frontmatter.tags.map((tag, index) => {
                            return <Tag key={index} text={tag} />
                        })}
                    </p>

                    <hr/>
                    {node.frontmatter.featuredImage ?
                        <Img sizes={node.frontmatter.featuredImage.childImageSharp.sizes} /> :
                        null
                    }
                    <p dangerouslySetInnerHTML={{ __html: node.excerpt }} />
                    </div>
                )
            })}
            </Layout>
        )
    }
}

export default BlogIndex

export const pageQuery = graphql`
query {
    site {
        siteMetadata {
            title
        }
    }
    allMdx(
        sort: {
            fields: [frontmatter___date],
            order: DESC
        },
        filter: {
            frontmatter: {
                published: {
                    eq: true
                }
            }
        }
    ) {
        edges {
            node {
                id
                excerpt(pruneLength: 200)
                fields {
                    slug
                }
                frontmatter {
                    title
                    author
                    tags
                    date(formatString: "DD MMMM YYYY", locale: "fr")
                    featuredImage {
                        childImageSharp{
                            sizes(maxWidth: 630) {
                                ...GatsbyImageSharpSizes
                            }
                        }
                    }
                }
            }
        }
    }
}
`
