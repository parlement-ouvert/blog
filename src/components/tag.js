import React from "react"
import tagStyles from "./tag.module.css"

class Tag extends React.Component {
    render() {
        return (
            <span className={tagStyles.tag}>{this.props.text}</span>
        )
    }
}

export default Tag
