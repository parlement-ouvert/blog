new d3pie("pieAN", {
	"header": {
		"title": {
			"text": "Composition politique de l\'Assemblée nationale",
			"fontSize": 24,
			"font": "open sans"
		},
		"subtitle": {
			"text": "(inscrits et apparentés au 22/02/2019)",
			"color": "#999999",
			"fontSize": 12,
			"font": "open sans"
		},
		"titleSubtitlePadding": 9
	},
	"footer": {
		"color": "#999999",
		"fontSize": 10,
		"font": "open sans",
		"location": "bottom-left"
	},
	"size": {
		"canvasWidth": 1000,
		"pieOuterRadius": "80%"
	},
	"data": {
		"sortOrder": "value-desc",
		"smallSegmentGrouping": {
			"enabled": true
		},
		"content": [
			{
				"label": "La République en Marche",
				"value": 306,
				"color": "#2484c1"
			},
			{
				"label": "Les Républicains",
				"value": 104,
				"color": "#0c6197"
			},
			{
				"label": "Mouvement Démocrate et apparentés",
				"value": 46,
				"color": "#4daa4b"
			},
			{
				"label": "UDI, Agir et Indépendants",
				"value": 29,
				"color": "#90c469"
			},
			{
				"label": "Socialistes et apparentés",
				"value": 29,
				"color": "#daca61"
			},
			{
				"label": "La France insoumise",
				"value": 17,
				"color": "#e4a14b"
			},
			{
				"label": "Libertés et Territoires",
				"value": 16,
				"color": "#e98125"
			},
			{
				"label": "Gauche démocrate et républicaine",
				"value": 16,
				"color": "#cb2121"
			},
			{
				"label": "Non inscrits",
				"value": 14,
				"color": "#830909"
			},
		]
	},
	"labels": {
		"outer": {
			"pieDistance": 32
		},
		"inner": {
			"hideWhenLessThanPercentage": 3
		},
		"mainLabel": {
			"fontSize": 11
		},
		"percentage": {
			"color": "#ffffff",
			"decimalPlaces": 0
		},
		"value": {
			"color": "#adadad",
			"fontSize": 11
		},
		"lines": {
			"enabled": true
		},
		"truncation": {
			"enabled": true
		}
	},
	"effects": {
		"pullOutSegmentOnClick": {
			"effect": "linear",
			"speed": 400,
			"size": 8
		}
	}
});
