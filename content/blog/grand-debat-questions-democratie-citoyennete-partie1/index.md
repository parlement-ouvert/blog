---
title: "Questions de démocratie et citoyenneté : l'organisation de la vie démocratique en France (partie 1)"
tags: ["Démocratie", "Citoyenneté", "Grand Débat"]
date: "2019-03-02T12:00:00.000"
author: "Clémence Réda"
published: true
---

import { PieChart } from './piechart'

Le but de ce type de billets est de proposer des articles qui donnent un aperçu détaillé des questions abordées lors du Grand débat, et, en particulier, suggérer des visualisations, des analyses, de la situation actuelle et des politiques en place.

# Aperçu : l'organisation de la vie démocratique en France

## Présentation

La [Constitution française du 4 octobre 1958](https://www.conseil-constitutionnel.fr/sites/default/files/as/root/bank_mm/constitution/constitution.pdf) régit la Cinquième république française, et en liste les lois *organiques*, c'est-à-dire le cadre juridique auquel les autres lois votées par le Parlement français (Assemblée nationale et Sénat). Le pouvoir se partage entre trois entités (principe de séparation des pouvoirs théorisé par Montesquieu dans [De l'esprit des lois](https://www.ecole-alsacienne.org/CDI/pdf/1400/14055_MONT.pdf)) : le pouvoir **exécutif** (le Gouvernement), le pouvoir **législatif** (Parlement), et l'autorité **juridictionnel** (ordres administratif et judiciaire). Les différents acteurs de la vie politique (aux niveaux exécutif et législatif) sont :

+ Le Président de la République, qui est élu depuis 1962 ([loi du 6 novembre 1962](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000684037)) au suffrage universel direct, uninomial majoritaire à deux tours. Autrement dit, tous les citoyens peuvent participer à l'élection présidentielle, leur vote est directement pris en compte pour le choix du président, qui est déterminé par la majorité des votes. Lors du premier tour, les deux candidats ayant le plus de voix sont sélectionnés pour le second tour, qui détermine le vainqueur par la majorité des voix exprimées lors du second scrutin. Depuis 2000 ([loi du 2 octobre 2000](https://www.legifrance.gouv.fr/jo_pdf.do?numJO=0&dateJO=20001003&numTexte=1&pageDebut=15582&pageFin=15582)), le mandat du président dure cinq ans. En cas de démission, de décès ou d'empêchement du président, c'est le Président du Sénat qui prend le relais de la Présidence de la République.

+ Le Gouvernement se compose du Premier ministre et de son cabinet (dont la liste actuelle se trouve sur cette [page](https://www.gouvernement.fr/composition-du-gouvernement)). Le Premier ministre, dont la fonction existe depuis 1959 ([Constitution française du 4 octobre 1958](https://www.conseil-constitutionnel.fr/sites/default/files/as/root/bank_mm/constitution/constitution.pdf)), est soit élu par le Parlement, soit directement nommé par le Président de la République. Il est généralement issu du parti majoritaire à l'Assemblée nationale. Le Président peut mettre fin à l'exercice du Premier ministre si ce dernier lui présente la démission du Gouvernement, ou à l'exercice d'autres ministres, sur proposition du Premier ministre (article 8 de la Constitution).

+ L'Assemblée nationale est composée de 577 élus, qui sont nommés directement par les citoyens français lors des élections législatives. Ces élections se tiennent dans chaque circonscription (liste des circonscriptions depuis 2010 [ici pour les Français sur le territoire national](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=F88704CE17423CE4F460C49124010C5B.tplgfr29s_1?cidTexte=LEGITEXT000006070239&idArticle=LEGIARTI000020950777&dateTexte=20190222&categorieLien=id#LEGIARTI000020950777), et [ici pour les Français hors de France](https://www.legifrance.gouv.fr/affichCode.do;jsessionid=F88704CE17423CE4F460C49124010C5B.tplgfr29s_1?cidTexte=LEGITEXT000006070239&idSectionTA=LEGISCTA000020950783&dateTexte=20190222&categorieLien=id#LEGISCTA000020950783)) toutes à la même date (sauf pour les régions d'Outre-mer), déterminée par le Conseil des ministres ([source](http://www2.assemblee-nationale.fr/qui/elections-legislatives-des-11-et-18-juin-2017#node_39089)). Le mandat d'un député dure cinq ans. L'Assemblée nationale peut être dissoute par le Président de la république ([source](https://www.vie-publique.fr/questions/dissolution-assemblee-nationale-arme-presidentielle.html)). Dans ce cas, de nouvelles élections doivent être organisées de vingt à quarante jours après la dissolution. Le Président de l'Assemblée nationale, élu par les députés en début de législature, supervise les débats, et les décisions portant sur le statut ou l'exercice d'un député ([source](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-organes-de-l-assemblee-nationale/le-president-de-l-assemblee-nationale)). Son mandat est similaire à la durée de la législature.

La composition actuelle (au 22 février 2019) des 577 députés en fonction de leur affiliation politique est la suivante ([source](http://www2.assemblee-nationale.fr/instances/liste/groupes_politiques/effectif)) :

<PieChart
  title={'Composition politique de l\'Assemblée nationale'}
  subtitle={'(inscrits et apparentés au 22/02/2019)'}
  data={[
	{
		"label": "La République en Marche",
		"value": 306,
		"color": "#2484c1"
	},
	{
		"label": "Les Républicains",
		"value": 104,
		"color": "#0c6197"
	},
	{
		"label": "Mouvement Démocrate et apparentés",
		"value": 46,
		"color": "#4daa4b"
	},
	{
		"label": "UDI, Agir et Indépendants",
		"value": 29,
		"color": "#90c469"
	},
	{
		"label": "Socialistes et apparentés",
		"value": 29,
		"color": "#daca61"
	},
	{
		"label": "La France insoumise",
		"value": 17,
		"color": "#e4a14b"
	},
	{
		"label": "Libertés et Territoires",
		"value": 16,
		"color": "#e98125"
	},
	{
		"label": "Gauche démocrate et républicaine",
		"value": 16,
		"color": "#cb2121"
	},
	{
		"label": "Non inscrits",
		"value": 14,
		"color": "#830909"
	},
   ]}
   width={600} height={400} margin={10}
/>

+ Le Sénat est composée de 348 sénateurs. Le mandat d'un sénateur est de six ans, et il est élu par les députés, sénateurs, conseillers régionaux, conseillers départementaux, conseillers municipaux ([source](http://www.senat.fr/role/senate.html)). Le Président du Sénat est chargé de superviser les débats, et est élu pour trois ans par les sénateurs.

La composition actuelle (au 22 février 2019) des 348 sénateurs en fonction de leur affiliation politique est la suivante ([source](http://www.senat.fr/senateurs/grp.html)) :

<PieChart
  title={'Composition politique du Sénat'}
  subtitle={'(inscrits, apparentés, rattachés au 22/02/2019)'}
  data={[
	{
		"label": "Groupe socialiste et républicain",
		"value": 74,
		"color": "#2484c1"
	},
	{
		"label": "Les Républicains",
		"value": 145,
		"color": "#0c6197"
	},
	{
		"label": "Groupe Union Centriste",
		"value": 51,
		"color": "#4daa4b"
	},
	{
		"label": "La République en Marche",
		"value": 23,
		"color": "#90c469"
	},
	{
		"label": "Socialistes et apparentés",
		"value": 22,
		"color": "#daca61"
	},
	{
		"label": "Rassemblement Démocratique et social européen",
		"value": 16,
		"color": "#e4a14b"
	},
	{
		"label": "Communiste républicain citoyen et écologiste",
		"value": 12,
		"color": "#e98125"
	},
	{
		"label": "Les Indépendants",
		"value": 5,
		"color": "#cb2121"
	},
   ]}
   width={600} height={400} margin={10}
/>

+ Le Conseil constitutionnel comprend neuf membres, dont le mandat est de neuf ans. Trois d’entre eux sont élus par le Président de la république, trois autres par le Président de l’Assemblée nationale, et trois derniers par le Président du Sénat ([source](https://www.conseil-constitutionnel.fr/le-conseil-constitutionnel)).

Lors du vote d'une loi, les lois sont discutées alternativement par les deux chambres parlementaires (Assemblée nationale et Sénat) ; le Conseil constitutionnel peut éventuellement être saisi si plus de soixante parlementaires (députés ou sénateurs), ou le Président de la République, ou le Premier ministre, ou l'un des Présidents des chambres estiment que certains points de la loi discutée ne sont pas conformes au cadre défini par la Constitution ([source](https://www.conseil-constitutionnel.fr/le-conseil-constitutionnel/comment-saisir-le-conseil-constitutionnel)).

# Pour aller plus loin

Si vous voulez modifier, ou ajouter des éléments à cet article (notamment au niveau du scrutin proportionnel, du cumul des mandats, ou du vote blanc), vous pouvez le faire sur cette [page](https://hackmd.io/AlpX6o_tThau3v7PEN9TWA#).
