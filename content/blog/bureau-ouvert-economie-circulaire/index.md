---
title: Retour sur le Bureau Ouvert spécial "Texte de loi Économie Circulaire" organisé avec la secrétaire d'Etat Brune Poirson le 26 avril 2019
date: "2019-05-10T10:00:00.000"
author: "Marion Dos Reis Silva"
featuredImage: "./thumbnail.jpg"
tags: ["Consultation citoyenne"]
published: true
---

A la demande de Brune Poirson, secrétaire d’Etat à la Transition écologique et solidaire, le Bureau Ouvert du vendredi 26 avril 2019 a été délocalisé dans les locaux de la secrétaire d’Etat afin de l’accompagner dans la réflexion et la mise en place d'un dispositif de participation citoyenne dans le cadre du texte de loi sur l'économie circulaire qu’elle porte.

# Une mobilisation forte et pertinente des membres de la communauté du Bureau Ouvert

Une trentaine d’expert.e.s de la participation citoyenne, issu.e.s des civic tech, de l’administration ou du secteur privé ont répondu présents.
La journée a commencé par une présentation des grandes lignes du texte de loi, des attentes en terme de participation citoyenne (type de participation, publics visés, impacts recherchés). Puis Paula Forteza a rappelé les différents types de participation (délibérative, participative, en ligne, présentielle) et a présenté des exemples français et internationaux très significatifs en termes de résultats comme par exemple la consultation en ligne sur le texte de Loi pour une République Numérique pendant sa discussion au Parlement, le mélange démocratie délibérative et participative en ligne à Taïwan, le processus de participation pour la révision de la Constitution de la ville de Mexico.

Les participants se sont ensuite répartis en quatre groupes de travail sur 4 thématiques :
- Affichage et information aux consommateurs
- Comment on trie et on collecte (quelles consignes)
- Les données et la transparence
- Plastique et emballage

# Après une journée de travail, une feuille de route précise et opérationnelle est née

A l’issue de la journée, une feuille de route a été conçue. Elle vise à intégrer des dispositifs de participation citoyenne au processus législatif et à la mise en application du projet de loi “Economie Circulaire” en proposant pour chaque thématique des pistes précises de participations citoyennes.

## Thème 1 : Affichage et information aux consommateurs

Ce groupe a travaillé sur la question de l’affichage et des informations à fournir au consommateur, et a proposé plusieurs dispositifs de participation citoyenne. Le retour concret des futurs utilisateurs (citoyens) permettra aux parlementaires, ainsi qu’au gouvernement, d’adapter les dispositifs d'information aux besoins réels (création d’indices, de labels, de moyens de communication, de pictogrammes etc.)

**Plusieurs problématiques, questions ou situations pourraient être testées auprès des citoyens :**
- De quelle information avez vous besoin pour acheter/jeter en toute conscience/transparence ?
- Quelle forme devrait prendre cette information ? Quel design résulte le plus efficace ?
- Témoignages sur les problèmes rencontrés au quotidien autour de la consommation/réparation/recyclage

**La participation des citoyens, des experts et des communautés ciblées pourra se faire à travers différentes méthodologies et à différents moments. Voici quelques exemples :**
- Un moment de délibération et d’intelligence collective en présentiel pour faire remonter le diagnostic, l'identification des manques ou la nature de l'information nécessaire.
    - World Café[¹](#note1) : discussions en petit groupes tournants dans une ambiance conviviale
    - Mini-Publics[²](#note2) : groupe de citoyens tirés au sort ou sélectionnées pour avoir un panel représentatif du public visé
- Une Plateforme numérique pour faire remonter les témoignages sur des problèmes liés à la réparation de produits (ex : produit défaillant et irréparable) et ainsi enrichir le débat sur l’indice de réparabilité des appareils électroménagers. Ceci pourrait nourrir une politique de name and shame.
- Un moment de co-création, d’expérimentation pour tester et designer les informations utiles et les supports plus adaptés :
    - Organisation d’un hackathon avec des designers, des experts en sciences du comportement pour designer l’affichage de l’indice de réparabilité mis en place par le PJL
    - Plateforme numérique de vote sur les 4 ou 5 projets de visuels qui sortiront du hackathon pour un retour plus large des citoyens
- Ateliers techniques avec communautés ciblées afin de cartographier et repérer les données à ouvrir, notamment en vue de l’établissement d’un indice de réparabilité.

## Thème 2 : Comment on trie et on collecte (quelles consignes)

Ce groupe a travaillé sur le sujet du tri et la collecte, en réfléchissant à l'interaction des citoyens avec le cycle des déchets afin de l'adapter au mieux aux besoins pour faciliter l’adoption des nouvelles mesures envisagées dans la loi. Les différents dispositifs de participation citoyenne permettront aux parlementaires, ainsi qu’au gouvernement, de comprendre les besoins réels, de consulter toutes les parties prenantes en vue d’une harmonisation des politiques publiques et surtout, d’inclure les citoyens dans l’ensemble du processus. Une partie des propositions envisage des outils de participation post-adoption de la loi pour encourager et faciliter la diminution du gaspillage chez les citoyens.

**Plusieurs problématiques, questions ou situations pourraient être testées auprès des citoyens :**
- Comment mieux trier à la maison et dans l’espace public ? De quelles informations avons nous besoin ?
- Peut-on imaginer des consignes de tri modulables et adaptables au besoins (écoles, bureaux, industrie, restauration, services, maison…) ?
- Est-ce que la collecte est efficace? Comment suivre et donc contrôler le cycle des déchets?
- En vue de l’installation de consignes dans l’espace public : quels produits seraient concernés?, où devons-nous installer les consignes?, que reçoit-on en contrepartie?
- Comment peut-on être acteurs de l’économie circulaire au quotidien?

**La participation des citoyens, des experts et des communautés ciblées pourra se faire à travers différentes méthodologies et à différents moments. Voici quelques exemples :**
- Un moment de délibération et d’intelligence collective en présentiel pour faire remonter le diagnostic, les besoins, les bonnes pratiques et/ou la nature de l'information nécessaire. Cette démarche a pour but de comprendre et améliorer l’expérience utilisateur autour du tri, du recyclage, et les consignes dans le but d’adapter les politiques publiques et s’assurer de leur adoption par les citoyens.
    - World Café[¹](#note1) : discussions en petit groupes tournants dans une ambiance conviviale
    - Mini-Publics[²](#note2) : groupe de citoyens tirés au sort ou sélectionnées pour avoir un panel représentatif du public visé
    - Consultations[³](#note3) : il semble important de consulter en parallèle les parties prenantes pour anticiper les changements et co-décider les politiques publics du tri (collectivités, régions, villes, entreprises, associations…)
- Encourager la participation et le contrôle citoyen dans le cycle de recyclage à travers plus de transparence. Une plateforme numérique pourrait permettre aux citoyens et associations de suivre le cycle des déchets pour démystifier le processus mais aussi encourager l’amélioration de ce service public.
- Donner les outils aux citoyens pour devenir acteurs de l’économie circulaire dans leur quotidien et lutter contre le gaspillage. Chaque année, le gouvernement pourrait lancer des “défis” sur une problématique spécifique et inviter les citoyens à développer des outils et des solutions. Cette idée est inspirée d’expériences en Finlande[⁴](#note4) et en Argentine[⁵](#note5) , qui permettent au gouvernement de co-créer avec la société civile la mise en place des politiques publiques.

## Thème 3 : Les données et la transparence

Ce groupe a travaillé sur le sujet des données et de la transparence, en réfléchissant à la conception, au suivi et à l’évaluation de la loi “anti-gaspi”. Il a d’ores et déjà identifié des jeux de données existants à ouvrir en priorité :
- les données ACV
- la base SINOÉ
- la base ADEM
- BNVD
- Cycle des déchets
- Stations de tri
- Cartographie par géographie et usages pour maximiser le tri et la collecte
- Manuels de réparation de produits type iPhone

**Plusieurs problématiques, questions ou situations pourraient être testées auprès des citoyens et publics ciblés :**
- De quelles données avons-nous besoin ? Sont-elles publiques, privés, ouvertes, fermés?
- Quelles données faudra-t-il collecter/créer pour compléter les données existantes?

**La participation des citoyens, des experts et des communautés ciblées pourra se faire à travers différentes méthodologies et à différents moments. Voici un exemple :**
- Consultation pour identifier et prioriser les données à ouvrir (données déjà existantes) et à créer/collecter (données manquantes) => les-donnees-que-nous-voulons.gouv.fr
    - Un espace de recensement des jeux de données (liste et ajout) et une fonction de vote pour identifier ceux qui sont les plus plébiscités.
    - Un “trello” pour suivre l’ouverture des jeux de données qui ont reçu 100 likes
    - Un engagement politique à organiser l’ouverture rapide des X jeux de données les plus plébiscitées


## Thème 4 : Plastique et emballage

Ce groupe a travaillé sur la transparence et l’ouverture du travail parlementaire, en réfléchissant à l'interaction et le suivi citoyen tout au long du processus de la loi (présentation du texte, Commission, séance, navette, deuxième lecture, adoption, suivi décrets d'application). La loi Economie Circulaire, et plus spécifiquement, la thématique “plastique et emballage” pourraient être l’occasion d’expérimenter un tel outil pour suivre la discussion d’un sujet controversé et qui fera l’objet de débats importants et intéressants.

**Plusieurs fonctionnalités pourraient être testées en utilisant l’outil Decidim (plateforme de participation citoyenne développé par la ville de Barcelone et utilisé par plusieures collectivités en France) :**  
- Suivi des différentes versions du texte législatif
- Suivi des amendements déposés (par les députés, par le gouvernement)
- Débat citoyen sur les amendements en parallèle de la discussion en commission et à l’hémicycle
- Suivi des décrets d’application

**Quelques conseils et pré-requis pour une participation citoyenne effective :**
- Respecter l’engagement du gouvernement vis-à-vis les participants et le processus
- Respecter la charte de la participation du public
- Impliquer toutes les bonnes volontés dans la co-construction puis la réalisation de cette consultation
- Faire appel aux ressources (conseils, outils marchés, etc) mises à disposition par l'interministériel (DITP, DINSIC), etc)
- La démarche de consultation est au moins aussi importante que son résultat (apprendre, se rencontrer, préparer la suite)
- Mise en open data des résultats de la consultation
- Documenter l’expérimentation pour :
    - débrief et amélioration
    - dépôt de dossier aux trophées de la participation
    - présentation aux “Mardi de la consultation”

## Références
<a id="note1">¹ http://www.theworldcafe.com</a><br />
<a id="note2" href="https://newdemocracy.com.au/wp-content/uploads/2017/05/docs_researchnotes_2017_May_nDF_RN_20170508_FormsOfMiniPublics.pdf">² https://newdemocracy.com.au/wp-content/uploads/2017/05/docs_researchnotes_2017_May_nDF_RN_20170508_FormsOfMiniPublics.pdf</a><br />
<a id="note3" href="https://consultation.etalab.gouv.fr">³ https://consultation.etalab.gouv.fr</a><br />
<a id="note4" href="https://www.foreigner.fi/articulo/business/finnish-government-allocates-4-million-euros-to-finance-innovative-circular-economy-projects/20190123114631001169.html">⁴ https://www.foreigner.fi/articulo/business/finnish-government-allocates-4-million-euros-to-finance-innovative-circular-economy-projects/20190123114631001169.html</a><br />
<a id="note5" href="http://desafiospublicos.argentina.gob.ar">⁵ http://desafiospublicos.argentina.gob.ar</a>
