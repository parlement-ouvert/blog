---
title: "(Grand débat) Questions sur la transition écologique (partie 1)"
tags: ["Écologie", "Climat et environnement", "Grand débat"]
date: "2019-03-20T12:00:00.000"
author: "Clémence Réda"
published: true
sources: []
external_sources: ["https://d3js.org/d3.v3.min.js"]
---

# Réchauffement climatique et pollution

La France est en retard au
regard de ses engagements pour atteindre
cet objectif

## Evolution de la température mondiale moyenne

http://www.meteofrance.fr/climat-passe-et-futur/le-rechauffement-observe-a-l-echelle-du-globe-et-en-france
[évolution de la température mondiale moyenne par année (point chart)]

## Le seuil critique de 2 °C de hausse de la température mondiale

https://e-rse.net/rapport-giec-2018-resume-explication-271275/#gs.1mhfvx
https://le-garde.fr/le-rapport-special-du-giec-du-8-octobre-2018/
https://www.greenpeace.fr/le-giec/

## Origine des émissions de gaz à effet de serre en France

https://www.lemonde.fr/planete/article/2018/01/23/gaz-a-effet-de-serre-la-france-sur-la-mauvaise-pente_5245904_3244.html

https://www.statistiques.developpement-durable.gouv.fr/emissions-nationales-de-gaz-effet-de-serre-0
https://www.statistiques.developpement-durable.gouv.fr/emissions-mondiales-de-gaz-effet-de-serre-0

D'après la fiche de contexte associée à cette thématique, "les énergies fossiles (charbon, pétrole, fioul, gaz) que nous consommons pour nous déplacer ou nous chauffer, représentent aujourd’hui 70 % des émissions de gaz à effet de serre (le reste provenant essentiellement de l’agriculture)".

# Politiques actuelles

novembre 2017 Accord de Paris sur le Climat : https://unfccc.int/sites/default/files/french_paris_agreement.pdf

D'après la fiche de contexte associée à cette thématique,

+ "Parvenir à zéro émissions nettes en 2050 (ne pas émettre plus de gaz à effet de serre que ce qui peut être absorbé par nos forêts et nos sols) tout en réduisant à moins de 50% la part de nucléaire dans la production d’électricité à échéance 2035". ?

https://www.cairn.info/revue-francaise-d-administration-publique-2010-2-page-205.htm

https://www.ecologique-solidaire.gouv.fr/comite-des-politiques-lenvironnement

## Réchauffement climatique

## Pollution de l'air

## Erosion du littoral

## Atteinte à la biodiversité

+ mise en place du tri sélectif
+ taxe carbone (+ théorie économie citation)
+ paris : jour de transports en commun
+ prime à la conversion
(jusqu’à 5000 € d’aide)
+ certificats d’économie d’énergie ; le crédit d’impôt de transition énergétique couvre 30 % des dépenses d’isolation des murs ou des planchers et les aides sont augmentées pour les ménages aux revenus modestes grâce aux aides de l’Agence nationale de l’habitat (Anah) et au dispositif de l’écoprêt à taux zéro (éco-PTZ).

# Pour aller plus loin

Si vous voulez modifier, ou ajouter des éléments à cet article (notamment sur les conséquences de la pollution et du réchauffement climatique sur l'environnement et la population, ou sur les initiatives locales pour l'environnement, ou sur la *précarité énergétique* - la situation des ménages qui investissent plus de 8 % de leurs revenus pour se chauffer - et les aides disponibles pour faciliter la transition énergétique, par exemple, le *chèque énergie* ([loi du 17 août 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031044385))), vous pouvez le faire sur cette [page](https://hackmd.io/m91itWvfQAWvU3_Y6Ifz4g#). 
