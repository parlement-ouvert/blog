import React from 'react'

export class Component extends React.Component {
  render() {
    return (
      <div className="shopping-list">
        <h3>Juste un composant de test for {this.props.name}</h3>
        <ul>
          <li>Bureau</li>
          <li>Ouvert</li>
        </ul>
      </div>
    );
  }
}
