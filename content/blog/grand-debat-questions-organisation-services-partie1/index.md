---
title: "(Grand débat) Questions d'organisation de l'Etat et des services publics (partie 1)"
tags: ["Organisation de l'Etat", "Services publics", "Grand débat"]
date: "2019-03-15T12:00:00.000"
author: "Clémence Réda"
published: false
sources: ["d3pie.js"]
external_sources: ["https://d3js.org/d3.v3.min.js"]
---

# Organisation de l'Etat 

## Découpage

La [constitution du 4 octobre 1958](https://www.conseil-constitutionnel.fr/le-bloc-de-constitutionnalite/texte-integral-de-la-constitution-du-4-octobre-1958-en-vigueur) définit les collectivités territoriales françaises dans son article 72 : "Les collectivités territoriales de la République sont les communes, les départements, les régions, les collectivités à statut particulier et les collectivités d'outre-mer [...]". Les autres échelons administratifs peuvent être institués "collectivités territoriales" via les lois organiques.

Au 1er janvier 2019, la France comptait 34 979 **communes** ([source](https://www.lagazettedescommunes.com/599743/avec-les-communes-nouvelles-la-france-passe-sous-la-barre-des-35-000-communes/)), 101 **départements** ([source](http://media.education.gouv.fr/file/SIAM/19/1/Codification-des-departements_125191.pdf), où le département de Mayotte (indicatif 976) a été omis, peut-être parce qu'il est un département avec des compétences régionales ([source](https://www.vie-publique.fr/decouverte-institutions/institutions/collectivites-territoriales/categories-collectivites-territoriales/quelles-sont-collectivites-territoriales-situees-outre-mer.html))), 14 **régions** (source : [loi du 16 janvier 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030109622&categorieLien=id)), 1 258 **EPCIs** (établissements publics de coopération intercommunale) ([source](https://www.collectivites-locales.gouv.fr/cartographie-des-epci-a-fiscalite-propre)), 2 **collectivités uniques**, 5 **collectivités d'outre-mer** (COM) et 3 **collectivités à statut particulier** ([source](https://www.vie-publique.fr/decouverte-institutions/institutions/collectivites-territoriales/categories-collectivites-territoriales/quelles-sont-collectivites-territoriales-situees-outre-mer.html)).

Les différents échelons de l'administration française sont décrits ci-dessus :

+ La **commune** est la plus petite subdivision administrative en France ([source](https://www.insee.fr/fr/metadonnees/definition/c1468)). Les communes actuelles sont les vestiges des anciennes délimitations de paroisses, et ont été créées en 1789 ([source](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-institutions-francaises-generalites/l-organisation-territoriale-de-la-france)). Le représentant du pouvoir exécutif dans une commune est le maire ([source](https://www.insee.fr/fr/metadonnees/definition/c1468)), et il est chargé de gérer le budget, d'employer les agents de la commune (gardes champêtres, agents de police municipale, ...) qui sont placés (à quelques exceptions près, par exemple, pour Paris et Lyon ([source](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-institutions-francaises-generalites/l-organisation-territoriale-de-la-france))) sous son autorité.

+ L'**intercommunalité**, ou **établissement public de coopération intercommunale** (EPCI), est un regroupement de communes. Elle peut être de différentes natures : *intercommunalité de gestion*, ou associative, (où les communes se rassemblent dans le but de mettre en commun la gestion de certains services publics ou de la réalisation d'équipements), et *intercommunalité de projet*, ou fédérative, (où les communes se rassemblent pour réaliser ensemble un projet d'impact local) ([source](https://www.collectivites-locales.gouv.fr/differents-groupements-intercommunaux)). Une différence essentielle entre ces deux types d'intercommunalité est au niveau du financement des projets : l'intercommunalité de projet peut recevoir des financements propres (indépendamment des communes qui en font partie), contrairement à l'intercommunalité de gestion, qui est financée par les communes membres ([source](https://www.vie-publique.fr/decouverte-institutions/institutions/collectivites-territoriales/intercommunalite-cooperation-locale/comment-definir-intercommunalite.html)). L'*intercommunalité de projet* s'appelle également établissement public *à fiscalité propre*. Ces types d'intercommunalité sont elles-mêmes découpées ; par exemple, l'*intercommunalité de projet* peut être une communauté de communes, communauté de communes à dotation globale de fonctionnement bonifiée, communauté d’agglomération, communauté urbaine, métropole, etc. ([source](https://www.collectivites-locales.gouv.fr/differents-groupements-intercommunaux))

+ Le **département** est le regroupement le plus ancien de communes, et est géré par le préfet, dont le champ d'action est déterminé par la [loi du 10 août 1871](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070209&dateTexte=19960223). Il est responsable de l'ordre public, et donc, en particulier, détient des pouvoirs de police ([source](https://www.vie-publique.fr/decouverte-institutions/institutions/collectivites-territoriales/principes-collectivites-territoriales/quelle-est-fonction-prefet.html)), à l'exception de certaines villes, comme Paris ou Lyon ([source](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-institutions-francaises-generalites/l-organisation-territoriale-de-la-france))).

+ La **région** vise à "donner davantage de cohérence à la politique de l’État, à un échelon supérieur à celui du département" ([source](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-institutions-francaises-generalites/l-organisation-territoriale-de-la-france))).

# Aperçu de l'état des services publics en France

https://www.collectivites-locales.gouv.fr/files/files/statistiques/brochures/les_cl_en_chiffres_2018.pdf 

(2018)

[Communes réparties selon leurs dépenses annuelles, leurs investissements annuels]

[Point chart communes selon leur effectif (fonctionnaires) en fonction du temps]

## Panorama de la fonction publique en France

D'après la fiche de contexte associée à cette thématique, la répartition actuelle des agents publics français se présente de la façon suivante :

![Répartition des agents publics](./repartitionAP.png)
([réalisation](https://www.chartgo.com/))

# Pour aller plus loin

Si vous voulez modifier (et notamment détailler les rôles de chaque type d'intercommunalité), ou ajouter des éléments à cet article (notamment les missions propres à chaque échelon administratif - par exemple, la compétence GEMAPI relative à la gestion des milieux aquatiques et de la prévention des inondations, qui incombe actuellement aux intercommunalités ([loi du 30 décembre 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=4A402ECC992503FB234C0022DCED4C6A.tplgfr24s_2?cidTexte=JORFTEXT000036339387&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000036339087)), ou sur l'accessibilité de l'administration : démarches via Internet, maisons de service au public, ...), vous pouvez le faire sur cette [page](https://hackmd.io/CRT77DXwSDW2iysbOlUwKg#). 
