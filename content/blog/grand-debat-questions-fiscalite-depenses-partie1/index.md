---
title: "(Grand débat) Questions de fiscalité et de dépenses publiques : répartition du budget, impôts et compétitivité (partie 1)"
tags: ["fiscalité", "dépenses", "grand débat"]
date: "2019-03-01T12:00:00.000"
author: "Clémence Réda"
published: false
---

# Dépenses de la France

Les dépenses des fonds publiques sont régies par les *lois des finances*. Une loi de finances est d'un des deux types suivants : soit *initiale*, et elle doit "[prévoir] et [autoriser] l’ensemble des ressources et des dépenses du budget de l’État pour l’année civile" qui arrive ; soit *rectificative*, et elle doit "modifie en cours d’exercice les dispositions de la loi de finances de l’année et de la loi de règlement, qui retrace les recettes et les dépenses telles qu’elles ont effectivement eu lieu" ([source](https://www.performance-publique.budget.gouv.fr/budget-comptes-etat/lois-finances#.XHlQlJyDPeQ)). Comme toute autre loi, elle est soumise et votée par les deux chambres parlementaires (Assemblée nationale et Sénat).

Le budget général de l'Etat, dont la dépense est décrite par les lois de finances, est constitué des recettes fiscales de l'Etat (autrement dit, des impôts). Les impôts (*directs*, comme par exemple l'impôt sur le revenu, et *indirects*, comme par exemple la TVA) représentent 90% des recettes de l'Etat, et s'élèvent à 273,5 milliards d’euros nets en 2019 ([source](https://www.performance-publique.budget.gouv.fr/budget-comptes-etat/budget-etat/approfondir/recettes-etat/recettes-fiscales#.XHlSoJyDPeQ)).

L'[application](https://budget.parlement-ouvert.fr) ci-contre retrace les dépenses décrites par les lois de finances depuis 2016 (avec les domaines de dépenses et le montant). On constate, en observant les données du PLF (Projet de loi de finances) 2019 présentées par cette application, la répartition des dépenses au niveau du budget général. Cinq grands domaines représentent plus de la moitié des dépenses (présentés dans l'ordre croissant du niveau de dépenses) :

+ **Le remboursement de la dette** (approx. 29,2 % des dépenses du budget général de l'Etat)

+ **L'enseignement primaire et secondaire (écoles primaires, collèges, lycées)** (15,6 %)

+ **La défense** (9,5 %)

+ **Les engagements financiers de l'Etat** (9,1 %)

+ **La recherche et l'enseignement supérieur** (6,0 %)

D'après les statistiques de l'Insee sorties en 2018, "en 2016, le taux de [prélèvements obligatoires](https://www.vie-publique.fr/decouverte-institutions/finances-publiques/ressources-depenses-etat/ressources/quels-sont-prelevements-obligatoires.html) des administrations publiques (APU) stagne à 44,4 % du produit intérieur brut (PIB), après une légère baisse durant deux années consécutives". Le graphique ci-dessous, issu de la même page, représente le poids (en % du PIB français) des prélèvements obligatoires depuis 60 ans :

![Prélèvements obligatoires depuis 1960](./inseePO.png)

D'après la [fiche de contexte](https://granddebat.fr/media/default/0001/01/6b2e1b9fd79827c0997584ccff32d2f43151a187.pdf) associée à cette thématique, "le taux de prélèvements obligatoires en France s’est élevé à 45,3 % de PIB en 2017".

# La dette publique française

La dette publique est la cumulation des emprunts du gouvernement, tandis que le socle structurel est la différence (calculée annuellement) entre les recettes et les dépenses du gouvernement ([source](https://www.lafinancepourtous.com/outils/questions-reponses/quelle-est-la-difference-entre-deficit-public-et-dette-publique/)). Si ce socle est négatif, alors il est appelé "déficit structurel". Suite à la crise financière de 2008 (crise des "subprimes"), l’Union Européenne a créé une *Procédure de Déficit Excessif* (PDE) dont la clôture était prévue pour 2018. Comme détaillé sur cette [page](https://www.ofce.sciences-po.fr/blog/plf-2018-fin-dune-procedure-debut-dune-nouvelle/), les Etats membres de la zone euro sortant d’une telle procédure doivent respecter les deux obligations suivantes :

 + "Avoir une cible de déficit structurel (c’est-à-dire après correction des effets de la conjoncture) au moins inférieure à 0,5 point de PIB potentiel. Cette cible est l’*Objectif de Moyen Terme (OMT)* de l’État membre" ;

 + "Avoir une dette publique inférieure à 60 % du PIB, ou qui est en train de converger vers cette cible à un horizon de 20 ans. Ceci est connu comme le respect du critère de dette".

 D'après la [loi de finances (initiale) 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037882341&categorieLien=id), le déficit public français est de 107,7 milliards d'euros, et l'amortissement de la dette à moyen et long terme pour 2019 (c'est-à-dire la partie à rembourser pour l'échéance courante) est de 130,2 milliards d'euros. Ces chiffres représentent respectivement 4,7 % et 5,7 % du PIB de la France au troisième trimestre de 2018 (qui est de 2 282,8 milliards d'euros [source](https://www.journaldunet.com/management/conjoncture/1040948-pib-de-la-france/)). La dette publique s'élevait fin 2018 à 2 322,3 milliards d'euros ([source](http://www.lefigaro.fr/economie/le-scan-eco/dessous-chiffres/2018/12/31/29006-20181231ARTFIG00046-croissance-pouvoir-d-achat-dette-quel-bilan-pour-la-france-en-2018.php)), donc 101,7 % du PIB en 2018.

 D'après cette [page](https://budget.parlement-ouvert.fr/visualisation?source=PLF&year=2019&code=PLF), le total de la dépense du budget général prévue pour 2019 s'élève donc à 20,3 % du PIB en 2018 (464 milliards d'euros).

 Donc la France ne respecte *a priori* pas les termes de la PDE actuellement.

# Pour aller plus loin

Si vous voulez modifier (notamment au niveau des sources ou des calculs), ou ajouter des éléments à cet article, notamment au niveau des comparaisons de la situation en France par rapport aux autres pays développés, des impôts actuellement prélevés en France, des taxes "qui poussent au bon comportement", comme la taxe carbone ou "soda" (introduite par la [loi du 28 décembre 2018](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025044460&categorieLien=id) et renforcée par l'article 19 de la [loi du 31 décembre 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=4A402ECC992503FB234C0022DCED4C6A.tplgfr24s_2?cidTexte=JORFTEXT000036339090&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000036339087)), le coût du travail en France, ou de théories d'économie pertinentes par rapport au sujet développé ici, vous pouvez le faire sur cette [page](https://hackmd.io/mW7qEMa7QuCMntflSzklQQ#).
